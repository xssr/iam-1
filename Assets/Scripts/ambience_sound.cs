﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ambience_sound : MonoBehaviour
{
    AudioSource audioSource1; // Переменная для Аудиосорса первого звука
    AudioSource audioSource2; // Переменная для Аудиосорса второго звука
    public AudioClip outdoorClip; // Перменная для хранения сэмпла звука
    public AudioClip indoorClip; // Перменная для хранения сэмпла звука
    [Range(0f, 1f)] public float AudioClip1Volume = 1f; // Создаем фейдер громкости для первого аудиоклипа
    [Range(0f, 1f)] public float AudioClip2Volume = 1f; // Создаем фейдер громкости для второго аудиоклипа
    public float fade = 3f; // Переменная для установки длительности кроссфейда звука

    void Start()
    {
        audioSource1 = gameObject.AddComponent<AudioSource>(); // Добавляем компонент Аудиосорс для переменной
        audioSource1.clip = outdoorClip ; // Добавляем сэмпл в поле клип аудиосорса
        audioSource1.loop = true; // Включаем галочку луп аудиосорса
        audioSource1.volume = 0f; // Устанавливаем громкость сорса равной нулю
        audioSource1.playOnAwake = false; // Убираем галочку playOnAwake
        audioSource1.Play(); // Запускаем звук
        
        audioSource2 = gameObject.AddComponent<AudioSource>(); // Добавляем компонент Аудиосорс для переменной
        audioSource2.clip = indoorClip; // Добавляем сэмпл в поле клип аудиосорса
        audioSource2.loop = true; // Включаем галочку луп аудиосорса
        audioSource2.volume = 0f; // Устанавливаем громкость сорса равной нулю
        audioSource2.playOnAwake = false; // Убираем галочку playOnAwake
        audioSource2.Play(); // Запускаем звук
        
        StartCoroutine(Fadein(audioSource1, AudioClip1Volume)); // https://habr.com/ru/post/216185/ Coroutine - способ запускать функции, которые должны работать параллельно в течение некоторого времени.
                                                                        //Выполняем фейдин для звука аудиосорса улицы
       
    }


    IEnumerator Fadein(AudioSource source, float volume) // Фукнция Fadein для переменных нужного аудиосорса и нужной громкости
    {
        
        while (source.volume < volume) // Пока громкость сорса не достигнет громкости, которая передается из переменной AudioClip1Volume или AudioClip2Volume
        {
            source.volume += (Time.deltaTime / fade); // Постепенно добавляем громкость
            yield return null;
        }
    }
    IEnumerator Fadeout(AudioSource source) // Фукнция Fadeout для переменных нужного аудиосорса и нужной громкости
    {
        
        while (source.volume > 0f)// Пока громкость сорса больше нуля, будем постепенно уменьшать громкость
        { source.volume -= (Time.deltaTime / fade);
           yield return null;
        }
        
    }

    private void OnTriggerEnter(Collider other) // Функция запускается, если что-то вошло в коллайдер
    {
        if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
        {
       
            StartCoroutine(Fadeout(audioSource1));  // Делаем фейдаут для звука улицы
            StartCoroutine(Fadein(audioSource2, AudioClip2Volume)); // Делаем фейдин для звука дома

        }
    }

    private void OnTriggerExit(Collider other) // Функция запускается, если что-то вышло из коллайдера
    {
        if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
        {
      
            StartCoroutine(Fadein(audioSource1, AudioClip1Volume)); // Делаем фейдин для звука улицы
            StartCoroutine(Fadeout(audioSource2)); // Делаем фейдаут для звука дома

        }
        
    }
}
