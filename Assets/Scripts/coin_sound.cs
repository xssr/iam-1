﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin_sound : MonoBehaviour
{
    private AudioSource source; // Переменная для аудиосорса
    public GameObject coin; // Переменная для гейм-объекта монетки
    public bool collided = false; // переменная-флажок для определения пересек ли герой коллайдер объекта
    public GameObject particles; // Переменная гейм-объекта для эффектов

    void Start()
    {
        source = gameObject.GetComponent<AudioSource>(); // Указываем путь к Аудиосорсу, который находится на гейм-объекте
    }

   
    private void OnTriggerEnter(Collider other) // Функция запускается, если что-то пересекло коллайдер
    {
       if (!collided) // Проверяем не было ли пересечения ранее = булевая переменная collided не равна true
       {
           if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
           {
               source.Play(); // Запускаем звук на Аудиосорсе
               particles.GetComponent<ParticleSystem>().Play(); // Ищем компонент в гейм-объекте particles под названием PArticleSystem и включаем эффект
               Destroy(coin); // Удаляем гейм-объект coin - исчезает монетка
               collided = true; // Пресваеваем переменной collided значетие true = мы пересекли коллайдер
           }
       }
       
       
    }
}
